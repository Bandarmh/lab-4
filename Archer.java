
package com.mycompany.factorybuilder;

public class  Archer implements Characters{
    private String name;
    private Avatar avatar;
    
    public Archer(String name){
        if (name == null ) {
            throw new IllegalArgumentException("Character must have a name");
        }
            this.name = name;
            this.avatar = new Avatar.Builder(SkinTone.LIGHT)
                    .withHairColor(HairColor.BROWN)
                    .withHairType(HairType.CURLY)
                    .withBodyType(BodyType.MUSCULAR)
                    .withFacialFeatures(FacialFeatures.CLEAN_SHAVEN).build();

    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public Avatar getAvatar() {
        return null;
    }

    @Override
    public void setAvatar(Avatar avatar) {

    }
}
